using UnityEngine;
using UnityEngine.VR;
using System.Collections;

public class ForceFly : MonoBehaviour {

	public float movementSpeed;
	public float turnSpeed;
	public float tiltSpeed;

	public WiimoteControl controller;
	private Rigidbody rb;

	private Vector3 dir;
	public Vector3 diff;

	private float leanFactor = 1f;
	public float leanRange = 0.3f;

	public float spinFactor = 0.0f;

	private bool isGateEntered = false;

    public AudioSource gatePassSound;

    public AudioSource soundGetAmmoTypeFire;
    public AudioSource soundGetAmmoTypeIce;
    public AudioSource soundGetAmmoTypeEarth;
    public AudioSource soundGetAmmoTypeElectric;

    public GameObject visor;

    void Start () 
	{
		dir = Vector3.forward;
		rb = GetComponent<Rigidbody>();
        
        Debug.Log("WiiMote fly enabled.");
	}

	void Update () 
	{
	}

	void FixedUpdate ()
	{
		Vector3 controllerDirection = controller.GetCorrectedDirection ();

		dir.y = controllerDirection.x;
		dir.x = -controllerDirection.y;

		diff = dir - Vector3.forward;

		diff.x *= tiltSpeed;// * controller.GetDirReliabilityFactor();
		diff.y *= turnSpeed;// * controller.GetDirReliabilityFactor();
	
		transform.Rotate(diff, Space.Self);

		Vector3 newRot = transform.eulerAngles;
		newRot.z = 0;
		if (newRot.x < 300 && newRot.x > 180) {
			newRot.x = 300;
		} else if (newRot.x > 60 && newRot.x < 180) {
			newRot.x = 60;
		} 

		transform.eulerAngles = newRot;

		if(VRDevice.isPresent) {
			UpdateLeanFactor ();
		}

		rb.AddForce(transform.forward * movementSpeed * leanFactor, ForceMode.VelocityChange);
	}

	void UpdateLeanFactor()
	{
		float leanPosition = InputTracking.GetLocalPosition(VRNode.CenterEye).z;
		if ( leanPosition > leanRange) {
			leanPosition = leanRange;
		} else if ( leanPosition < -leanRange) {
			leanPosition = -leanRange;
		}

		leanFactor = leanPosition / leanRange;

		//Debug.Log ( InputTracking.GetLocalPosition (VRNode.CenterEye).z.ToString("0.0") +"     L:" + leanFactor.ToString("0.0"));
	}

	void OnTriggerEnter(Collider other) 
	{
		if (other.tag == "Gate") {

			if (other.gameObject.GetComponent<Gate> ().isOpen == true) {
				//Destroy (other.gameObject);
				//Debug.Log ("Gate crossed");
				other.gameObject.SendMessage ("getScore");
				Debug.Log ("get passed");
                //PUT HERE THE SCORE CODE
                gatePassSound.Play();
			} 
		}

        if (other.tag == "Ammo")
        {

            Debug.Log("enter Ammo");

            switch (other.GetComponent<Ammo>().ammoType)
            {
                case 1:
                    if (GlobalVariables.ammoType != 1)
                    {
                        soundGetAmmoTypeFire.Play();
                        visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 0f, 0f, 0.05f));
                    }
                    GlobalVariables.ammoType = 1;
                    break;
                case 2:
                    if (GlobalVariables.ammoType != 2)
                    {
                        soundGetAmmoTypeIce.Play();
                        visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(0f, 0.7f, 1f, 0.05f));
                    }
                    GlobalVariables.ammoType = 2;
                    break;
                case 3:
                    if (GlobalVariables.ammoType != 3)
                    {
                        soundGetAmmoTypeEarth.Play();
                        visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(0f, 1f, 0f, 0.05f));
                    }
                    GlobalVariables.ammoType = 3;
                    break;
                case 4:
                    if (GlobalVariables.ammoType != 4)
                    {
                        soundGetAmmoTypeElectric.Play();
                        visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 0f, 0.03f));
                    }
                    GlobalVariables.ammoType = 4;
                    break;
            }
        }

    }
}
