﻿using UnityEngine;
using System.Collections;

public class MaximumFlightHeight : MonoBehaviour {

	public GameObject player;
	public float maxHeight = 1050.0f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
		if (player.transform.position.y >= maxHeight) 
		{
			player.GetComponent<Rigidbody> ().AddForce (-Vector3.up * 10, ForceMode.Impulse);
		}
	}
}
