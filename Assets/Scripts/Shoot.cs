﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour {

	public GameObject bullet1;
	public GameObject bullet2;
	public GameObject bullet3;
	public GameObject bullet4;

	public AudioClip bulletshot1;
	public AudioClip bulletshot2;
	public AudioClip bulletshot3;
	public AudioClip bulletshot4;

	private AudioSource bulletsource;

	public WiimoteControl controller;

	public double reloadTime = 1.0f;
	private double waitingTime = 0.0f;
	private bool hasShot = false;

	// Use this for initialization
	void Start () {
		bulletsource = GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update () {

		if (waitingTime > 0.0f) {
			waitingTime -= Time.deltaTime;

		} else if (Input.GetKeyDown (KeyCode.PageUp) || Input.GetKeyDown (KeyCode.PageDown) || Input.GetMouseButtonDown (0) || Input.GetKeyDown ("space") || controller.IsButtonPressed (WiimoteControl.Button.TWO)) {

			waitingTime = reloadTime;

			switch (GlobalVariables.ammoType) {
			case 1:
				Instantiate (bullet1, this.transform.position, this.transform.rotation);
				bulletsource.PlayOneShot (bulletshot1);
				break;
			case 2:
				Instantiate (bullet2, this.transform.position, this.transform.rotation);
				bulletsource.PlayOneShot (bulletshot2);
				break;
			case 3:
				Instantiate (bullet3, this.transform.position, this.transform.rotation);
				bulletsource.PlayOneShot (bulletshot3);
				break;
			case 4:
				Instantiate (bullet4, this.transform.position, this.transform.rotation);
				bulletsource.PlayOneShot (bulletshot4);
				break;
			}

			hasShot = true;
		} else {
			hasShot = false;
		}
	}

	public bool isShooting () {
		return hasShot;
	}
}
