﻿using UnityEngine;
using System.Collections;

public class Grow : MonoBehaviour {

	//public float threshold = 100f;

	public float scale = 1.0f;
	public float rotate = 1.0f;
	public float livespan = 1.5f;

	private Vector3 initialScale;

	void Start () {
		Destroy (this.gameObject, livespan);

		Debug.Log ("Create Object");

		//initialScale = transform.localScale;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		
//		if (transform.localScale.x > threshold) 
//		{
//			transform.localScale = initialScale;
//		} else 
//		{
//			//transform.localScale += ((transform.localScale.x * transform.localScale.x)/100.0f) * (0.01f * scale * Vector3.one);
//			transform.localScale += 10 * scale * Vector3.one;
//		}

		transform.localScale += 10 * scale * Vector3.one;
		//transform.Rotate (Vector3.one * rotate);

		transform.rotation = Random.rotation;
	}
}
