﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Text;
using System;
using WiimoteApi;

public class WiimoteControl : MonoBehaviour {

	public enum Button
	{
		A = 0,
		B,
		ONE,
		TWO,
	}

    public RectTransform[] ir_dots;
    public RectTransform ir_pointer;

	private bool irValid;

    private Wiimote wiimote;

	//public Vector3 wmpOffset = Vector3.forward;
	private Vector3 accelPlusOffset = Vector3.zero;
	private Vector3 accelPlusDir = Vector3.forward;
	public float accelPlusCalibX = 0.03f;
	public float accelPlusCalibY = 0.03f;
	public Vector2 scaleAccelData = Vector2.one;

	private Vector2 initialPointerPos = Vector2.zero;
	private Vector2 pointerPos = Vector2.zero;

	float resetTimeYaw = 0.0f;
	float resetTimePitch = 0.0f;

	public float interpolation = 0.5f;
	public float interpol;

    void Start() {
		SetupWMPController ();

    }

	bool SetupWMPController () {
		WiimoteManager.FindWiimotes();

		if (!WiimoteManager.HasWiimote ()) 
			return false;

		wiimote = WiimoteManager.Wiimotes [0];

		wiimote.RequestIdentifyWiiMotionPlus ();

		wiimote.SendDataReportMode(InputDataType.REPORT_BUTTONS_ACCEL_IR10_EXT6);
		wiimote.SetupIRCamera (IRDataType.BASIC);
		wiimote.SendPlayerLED (true, false, false, false);
		wiimote.ActivateWiiMotionPlus (); 

		UpdatePointerPos ();

		initialPointerPos = pointerPos;

		return true;
	}

	public void ResetWMP() {
		MotionPlusData data = wiimote.MotionPlus;
		data.SetZeroValues();
	}

	void FixedUpdate () {

		if (Input.GetKeyDown (KeyCode.F))
			SetupWMPController ();

		if (Input.GetKeyDown (KeyCode.R)) {
			ResetWMP ();
		}

		if (!WiimoteManager.HasWiimote ())
			return;

		/**
		 *  WiiMote Plus poll data
		 */
        int ret;
        do
        {
            ret = wiimote.ReadWiimoteData();

            if (ret > 0 && wiimote.current_ext == ExtensionController.MOTIONPLUS) {
//                offset = new Vector3(  -wiimote.MotionPlus.PitchSpeed,
//                                        wiimote.MotionPlus.YawSpeed,
//                                        wiimote.MotionPlus.RollSpeed) / 95f; 
//			
				// Divide by 95Hz (average updates per second from wiimote)
				accelPlusOffset = new Vector3(wiimote.MotionPlus.PitchSpeed, -wiimote.MotionPlus.YawSpeed,0.0f) / 95f;

				accelPlusOffset.x += accelPlusCalibX;
				accelPlusOffset.y += accelPlusCalibY;

				if((float) Math.Round(accelPlusOffset.y, 2) == 0.0f)
					accelPlusOffset.y = (float) Math.Round(accelPlusOffset.y, 2);

				accelPlusOffset.x *= scaleAccelData.x;
				accelPlusOffset.y *= scaleAccelData.y;

				//wmpOffset += offset;

				bool pitchSlow = wiimote.MotionPlus.PitchSlow;
				bool yawSlow = wiimote.MotionPlus.YawSlow;
				bool rollSlow = wiimote.MotionPlus.RollSlow;

				//transform.Rotate (offset);
				accelPlusDir = Quaternion.Euler(accelPlusOffset) * accelPlusDir;
				accelPlusDir.Normalize();
            }
        } while (ret > 0); 
			
		/**
		 *  Acceleration data
		 */
		//Vector3 accelV = GetAccelVector ();

		/**
		 *  InfraRed data
		 */
		if (ir_dots.Length < 4) return;

        float[,] ir = wiimote.Ir.GetProbableSensorBarIR();
        for (int i = 0; i < 2; i++)
        {
            float x = (float)ir[i, 0] / 1023f;
            float y = (float)ir[i, 1] / 767f;
            if (x == -1 || y == -1) {
                ir_dots[i].anchorMin = new Vector2(0, 0);
                ir_dots[i].anchorMax = new Vector2(0, 0);
            }

            ir_dots[i].anchorMin = new Vector2(x, y);
            ir_dots[i].anchorMax = new Vector2(x, y);

        }

        float[] pointer = wiimote.Ir.GetPointingPosition();
        ir_pointer.anchorMin = new Vector2(pointer[0], pointer[1]);
        ir_pointer.anchorMax = new Vector2(pointer[0], pointer[1]);

		UpdatePointerPos ();

		/**
		 *  Reset accelerometer
		 */
		if (resetTimeYaw > 0.0f) {
			resetTimeYaw -= Time.deltaTime;
		} else if (pointer [0] >= 0.45f && pointer [0] <= 0.55f) {
			accelPlusDir.x = 0;
			resetTimeYaw = 2.0f ;
		}

		if (resetTimePitch > 0.0f) {
			resetTimePitch -= Time.deltaTime;
		} else if (pointer [1] >= 0.45f && pointer [1] <= 0.55f) {
			accelPlusDir.y = 0;
			resetTimePitch = 2.0f ;
		} 
	}

    void OnDrawGizmos()
    {
        if (wiimote == null) return;

		Gizmos.color = Color.blue;
		Gizmos.DrawLine(transform.position, transform.position + accelPlusDir * 2);

		Gizmos.color = Color.yellow;
		Gizmos.DrawLine(transform.position, transform.position + GetAccelVector() * 2);

		if (irValid) {
			Gizmos.color = Color.red;
			Gizmos.DrawLine (transform.position, transform.position + GetPointerDirection () * 2);
		}

		Gizmos.color = Color.green;
		Gizmos.DrawLine(transform.position, transform.position + GetCorrectedDirection() * 10);
    }

    private Vector3 GetAccelVector()
    {
        float accel_x;
        float accel_y;
        float accel_z;

        float[] accel = wiimote.Accel.GetCalibratedAccelData();
        accel_x = accel[0];
        accel_y = -accel[2];
        accel_z = -accel[1];

        return new Vector3(accel_x, accel_y, accel_z).normalized;
    }

	public Vector3 GetCorrectedDirection() {

		interpolation = Mathf.Clamp (interpolation, 0, 1);
			
		interpol = interpolation * GetDirReliabilityFactor();

		Vector3 dir = GetPointerDirection () * interpol + accelPlusDir * (1 - interpol);

		return dir.normalized;
	}

	public float GetDirReliabilityFactor() {
		float irInterpolationX = 1.0f - 1.0f * (pointerPos.x * pointerPos.x);
		float irInterpolationY = 1.0f - 1.0f * (pointerPos.y * pointerPos.y);

		return irInterpolationX * irInterpolationY;
	}

	private void UpdatePointerPos()
	{
		float[] pointer = wiimote.Ir.GetPointingPosition();
		if (pointer [0] > 1.0f || pointer [0] < 0.0f || pointer [1] > 1.0f || pointer [1] < 0.0f) {
			irValid = false;
			return;
		}

		irValid = true;
		
		pointerPos.x = pointer [0];
		pointerPos.y = pointer [1];
		pointerPos = (pointerPos - new Vector2(0.5f, 0.5f)) * 2.0f;
		//pointerPos -= initialPos;
	}

	private Vector3 GetPointerDirection()
	{
		return new Vector3( pointerPos.x, pointerPos.y, 1 ).normalized;
	}

	public bool IsButtonPressed(WiimoteControl.Button btn)
	{
		if (enabled && wiimote != null) {

			switch (btn) {
			case Button.A:
				return wiimote.Button.a;
			case Button.B:
				return wiimote.Button.b;
			case Button.ONE:
				return wiimote.Button.one;
			case Button.TWO:
				return wiimote.Button.two;
			}
		}
		return false;
	}

	void OnApplicationQuit() {
		if (wiimote != null) {
			WiimoteManager.Cleanup(wiimote);
	        wiimote = null;
		}
	}
}
