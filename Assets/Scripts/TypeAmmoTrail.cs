﻿using UnityEngine;
using System.Collections;

public class TypeAmmoTrail : MonoBehaviour {

	public int typeAmmo;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (typeAmmo == GlobalVariables.ammoType) {
			this.gameObject.transform.GetChild(0).gameObject.SetActive (true);
			this.gameObject.transform.GetChild(1).gameObject.SetActive (true);
			this.gameObject.transform.GetChild(2).gameObject.SetActive (true);
			this.gameObject.transform.GetChild(3).gameObject.SetActive (true);
		} else {
			this.gameObject.transform.GetChild(0).gameObject.SetActive (false);
			this.gameObject.transform.GetChild(1).gameObject.SetActive (false);
			this.gameObject.transform.GetChild(2).gameObject.SetActive (false);
			this.gameObject.transform.GetChild(3).gameObject.SetActive (false);
		}
	}
}
