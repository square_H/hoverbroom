﻿using UnityEngine;
using System.Collections;

public class Tutorial : MonoBehaviour {

	private Shoot shoot;
	public GameObject player;
	public GameObject iceGatePrefab;
	public GameObject iceAmmoPrefab;

	private float angle;

	private delegate void ActivateLevel();

	// Tutorial modes
	private bool isStepFlyingUp = false;
	private bool isStepFlyingDown = false;
	private bool isStepFlyingRight = false;
	private bool isStepFlyingLeft = false;
	private bool isStepSpeedUp = false;
	private bool isStepSlowDown = false;
	private bool isStepShooting = false;
	private bool isStepChangeArmour = false;
	private bool isStepShootIceGate = false;
	private bool isSpeedingUp = false;
	private bool isSlowedDown = false;

	private bool isRelocateIceGate = true;
	private GameObject iceGate;
	public float objectDistance = 200;

	private bool isRelocateIceArmour = true;
	private GameObject iceArmour;

	private bool isTimerActivated = false;
	private float timeLeft = 0;
	private bool isCountdownTimerActivated = false;
	private float countdownTimeLeft = 0;

	private float leanTimeLeft = 2;

	// Directions
	private Vector3 currentFlyingDirectionUp;
	private Vector3 currentFlyingDirection;
	private Vector3 pos;
	private bool isFlyingUp = true;
	private bool isFlyingDown = true;
	private bool isFlyingLeft = true;
	private bool isFlyingRight = true;

	private bool isUpdatePosition = true;

	private const float UPPERLIMIT = 1050.0f;

	public GameObject tutorialOrigin;
	public GameObject scoreLabel;
	public GameObject timeLabel;
	public GameObject placeholder;

    // Instructional pictures
    public GameObject picGameObject;
    public Texture picLeanForwards;
    public Texture picLeanBackwards;
    public Texture picTurnLeft;
    public Texture picTurnRight;
    public Texture picHoldNeutral;
    public Texture picHoldDown;
    public Texture picHoldUp;
    public Texture picEmpty;

	public bool isTutorial = true;

    // Use this for initialization
    void Start () {
		shoot = player.GetComponent<Shoot>();

		iceGate = Instantiate (iceGatePrefab);
		iceArmour = Instantiate(iceAmmoPrefab);
		isTimerActivated = true;
		timeLeft = 10;
		setLabelText ("Hold your broom in the neutral position\nto fly straight");
        picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picHoldNeutral);

        if (useLeanFactor) {
			isStepSpeedUp = true;
		} else {
			isStepFlyingUp = true;
		}

		if (!isTutorial)
			endTutorial ();
	}

	public float angleUp = 0.0f;
	public bool usePlayerPrepareState = false;
	public bool useLeanFactor = true;

	// Update is called once per frame
	void Update () {



//		if (player.transform.position.y >= UPPERLIMIT) {
//			Debug.Log ("max height");
//			player.transform.position = new Vector3(player.transform.position.x, UPPERLIMIT, player.transform.position.z);
//		}

		if (isCountdownTimerActivated) {
			countdownTimeLeft -= Time.deltaTime;
			if (countdownTimeLeft < 0) {
				endTutorial ();
			} else {
				setLabelText ( ((int)countdownTimeLeft).ToString());
			}
		}
		else if (isTimerActivated) {
			timeLeft -= Time.deltaTime;
			if (timeLeft < 0) {
				isTimerActivated = false;
			}
			currentFlyingDirectionUp = player.transform.up;
			currentFlyingDirection = player.transform.right;
			pos = player.transform.position;
		} else if (isStepFlyingUp) {
			setLabelText ("Raise your broom up to fly up");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picHoldUp);
            isFlyingUp = currentFlyingDirection.normalized.y < player.transform.forward.normalized.y;
			isUpdatePosition = !isFlyingUp;

			if(usePlayerPrepareState)
				angleUp = Vector3.Dot (player.transform.forward, currentFlyingDirectionUp);//CalculateAngle (player.transform.up, currentFlyingDirectionUp, Vector3.up);
			else
				angleUp = Vector3.Dot (player.transform.forward, Vector3.up);//CalculateAngle (player.transform.up, currentFlyingDirectionUp, Vector3.up);

			if (angleUp > 0.3f) {//10.0f) {
				isStepFlyingUp = false;
				activateFeeback ();
				isStepFlyingDown = true;
				isUpdatePosition = true;
			}
		} else if (isStepFlyingDown) {
			setLabelText ("And now push your broom down to fly down");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picHoldDown);
            isFlyingDown = currentFlyingDirection.normalized.y > player.transform.forward.normalized.y;
			isUpdatePosition = !isFlyingDown;

			if(usePlayerPrepareState)
				angleUp = Vector3.Dot (player.transform.forward, currentFlyingDirectionUp);
			else
				angleUp = Vector3.Dot (player.transform.forward, Vector3.up);

			if (angleUp <= -0.3f){//CalculateAngle(player.transform.up, currentFlyingDirectionUp, Vector3.up) < -10.0f) {
				isStepFlyingDown = false;
				activateFeeback ();
				isStepFlyingRight = true;
				isUpdatePosition = true;
			}
		} else if (isStepFlyingRight) {
			setLabelText ("Steer your broom to the right to turn right");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picTurnRight);
            isFlyingRight = AngleDir(currentFlyingDirection, player.transform.right, Vector3.up) == 1f;
			isUpdatePosition = !isFlyingRight;
			if (CalculateAngle(player.transform.right, currentFlyingDirection, Vector3.right) > 10.0f) {
				isStepFlyingRight = false;
				activateFeeback ();
				isStepFlyingLeft = true;
				isUpdatePosition = true;
			}
		} else if (isStepFlyingLeft) {
			setLabelText ("And steer your broom to the left to turn left");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picTurnLeft);
            isFlyingLeft = AngleDir(currentFlyingDirection, player.transform.right, Vector3.up) == -1f;
			isUpdatePosition = !isFlyingLeft;
			if (CalculateAngle(player.transform.right, currentFlyingDirection, Vector3.right) < -10.0f) {
				isStepFlyingLeft = false;
				activateFeeback ();
				isStepShooting = true;
			}
		} else if (isStepSpeedUp) {
			setLabelText ("Lean forwards to speed up");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picLeanForwards);

            if (leanTimeLeft < 0) {

				Vector3 movement = (player.transform.position - pos);

				if (Vector3.Dot(player.transform.forward, movement) > 0.0f) { 
					isStepSpeedUp = false;
					activateFeeback ();
					isStepFlyingUp = true;
				} else {
					pos = player.transform.forward;
				}

				leanTimeLeft = 2;
			} else {
				leanTimeLeft -= Time.deltaTime;
			}
		} else if (isStepSlowDown) {
			setLabelText ("And lean backwards to slow down again\nor go backwards");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picLeanBackwards);

            leanTimeLeft -= Time.deltaTime;
			if (leanTimeLeft < 0) {

				Vector3 movement = (player.transform.position - pos);

				if (Vector3.Dot(player.transform.forward, movement) < 0.0f) {
					isStepSlowDown = false;
					activateFeeback ();
					isStepFlyingUp = true;
				} else {
					pos = player.transform.forward;
				}

				leanTimeLeft = 2;
		}
		} else if (isStepShooting) {
			setLabelText ("Now press the button to shoot");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picEmpty);
            if (shoot.isShooting()) {
				isStepShooting = false;
				activateFeeback ();
				isStepChangeArmour = true;
			} 
		} else if (isStepChangeArmour) {
			setLabelText ("Your broom is loaded with Fire. \n Fly through the ice cloud to change it to ice");
            picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picEmpty);
            Vector3 playerPos = player.transform.position;
			Vector3 toGate = new Vector2(iceArmour.transform.position.x -  playerPos.x, iceArmour.transform.position.z -  playerPos.z ).normalized;
			if( Vector3.Dot(toGate, new Vector2(player.transform.forward.x, player.transform.forward.z)) < 0 ) {
				
				isRelocateIceArmour = true;
			}
			if (isRelocateIceArmour) {
				Vector3 iceArmourPos = playerPos + player.transform.forward * objectDistance;

				iceArmourPos.y = Mathf.Max (iceArmourPos.y, 1005);
				iceArmourPos.y = Mathf.Min (iceArmourPos.y, UPPERLIMIT - 5);

				iceArmour.transform.position = iceArmourPos;
				Vector3 v = new Vector3(player.transform.forward.x, 90, player.transform.forward.z);
				iceArmour.transform.rotation = Quaternion.FromToRotation(Vector3.up, v);
				isRelocateIceArmour = false; 
			}
			if (GlobalVariables.ammoType == 2) {
				isStepChangeArmour = false;
				activateFeeback ();
				isStepShootIceGate = true;
			} 
		} else if (isStepShootIceGate) {

			if (iceGate == null || !iceGate.activeInHierarchy) {

				Debug.Log ("Gate Shot!");

				isStepShootIceGate = false;
				activateFeeback ("You finished the tutorial! \nGet ready for the arena!");
                picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picEmpty);
            } else {
			
				setLabelText ("Use your ice ammo and shoot the ice gate to open it. \n Then fly through it to score!");
                picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picEmpty);
                Vector3 playerPos = player.transform.position;
				Vector3 toGate = new Vector2(iceGate.transform.position.x -  playerPos.x, iceGate.transform.position.z -  playerPos.z ).normalized;
				if( Vector3.Dot(toGate, new Vector2(player.transform.forward.x, player.transform.forward.z)) < 0 ) {
					isRelocateIceGate = true;
				}
				if (isRelocateIceGate) {
					Vector3 iceGatePosition = playerPos + player.transform.forward * objectDistance;

					iceGatePosition.y = Mathf.Max (iceGatePosition.y, 1010);
					iceGatePosition.y = Mathf.Min (iceGatePosition.y, UPPERLIMIT - 10);

					iceGate.transform.position = iceGatePosition;
					Vector3 v = new Vector3(player.transform.forward.x, 0, player.transform.forward.z);
					iceGate.transform.rotation = Quaternion.FromToRotation(Vector3.up, v);
					isRelocateIceGate = false; 
				}
			}
		} else {
			endTutorial ();
		}

		if (isUpdatePosition) {
			currentFlyingDirectionUp = player.transform.up;
			currentFlyingDirection = player.transform.right;
			isUpdatePosition = false;
		}
	}

	void endTutorial() {
		Destroy (placeholder);

		player.transform.position = Vector3.zero;
		GlobalVariables.score = 0;

		tutorialOrigin.SetActive (false);
		scoreLabel.SetActive (true);
		timeLabel.SetActive (true);

        picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picEmpty);

        this.gameObject.SetActive (false);
	}

	void activateFeeback(string message = "Very good!") {
        picGameObject.GetComponent<Renderer>().material.SetTexture("_MainTex", picEmpty);
        setLabelText (message);
		isTimerActivated = true;
		timeLeft = 3;
	}

	void setLabelText(string labelText) {
		GetComponent<TextMesh> ().text = labelText ;
	}

	float CalculateAngle(Vector3 from, Vector3 to, Vector3 n)
	{
		float angle = Vector3.Angle(from,to);
		float sign = Mathf.Sign(Vector3.Dot(n,Vector3.Cross(from,to)));

		// angle in [-179,180]
		float signedAngle = angle * sign;

		return signedAngle;
	}

	float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);

		if (dir > 0f) {
			return 1f; // right
		} else if (dir < 0f) {
			return -1f; // left
		} else {
			return 0f;
		}
	}

}
