﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
[RequireComponent (typeof(Camera))]
[AddComponentMenu ("Image Effects/Hoverbroom/Pulse")]
public class ScreenSpaceEffect : MonoBehaviour {

	public Shader maskShader;
	public Shader pulseShader;
	public Shader bloomShader;

	private Material maskMaterial;
	private Material pulseMaterial;
	private Material bloomMaterial;

	public enum Resolution
	{
		Low = 0,
		High = 1,
	}

	public enum BlurType
	{
		Standard = 0,
		Sgx = 1,
	}

	[Header("Ammo")]

	public Color color1 = Color.red;
	public Color color2 = Color.blue;
	public Color color3 = Color.green;
	public Color color4 = Color.yellow;

	public Texture2D pattern1;

	[Range(0f, 1f)]
	public float patternSize1 = 1;

	public Texture2D pattern2;

	[Range(0f, 1f)]
	public float patternSize2 = 1;

	public Texture2D pattern3;

	[Range(0f, 1f)]
	public float patternSize3 = 1;

	public Texture2D pattern4;

	[Range(0f, 1f)]
	public float patternSize4 = 1;

	[Header("Bloom Options")]

	public bool useBloom = true;

	[Range(0.0f, 1.5f)]
	public float threshold = 0.25f;

	[Range(0.0f, 2.5f)]
	public float intensity = 0.75f;

	[Range(0.25f, 5.5f)]
	public float blurSize = 1.0f;

	Resolution resolution = Resolution.Low;
	[Range(1, 4)]
	public int blurIterations = 1;
	public BlurType blurType= BlurType.Standard;


	[Header("Pulse Options")]

	[Range(0f, 1f)]
	public float pulseIntensity = 0.5f;

	[Range(0f, 1f)]
	public float uvShift = 0;

	[Range(0.25f, 5.5f)]
	public float pulseBlurSize = 1.0f;

	[Range(0, 4)]
	public int pulseBlurIterations = 1;

	[Range(0, 30000)]
	public int frame = 1;

	private Texture2D pattern;
	private Color pulseColor = Color.white;
	private float patternSize = 1;

	Material pMaterial
	{
		get
		{
			if (pulseMaterial == null) 
			{
				pulseMaterial = new Material (pulseShader);
				pulseMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return pulseMaterial;
		}
	}

	Material bMaterial
	{
		get
		{
			if (bloomMaterial == null) 
			{
				bloomMaterial = new Material (bloomShader);
				bloomMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return bloomMaterial;
		}
	}

	Material mMaterial
	{
		get
		{
			if (maskMaterial == null) 
			{
				maskMaterial = new Material (maskShader);
				maskMaterial.hideFlags = HideFlags.HideAndDontSave;
			}
			return maskMaterial;
		}
	}

	void OnEnable()
	{
		Camera cam = GetComponent<Camera>();
		if (cam != null) {
			cam.depthTextureMode = DepthTextureMode.Depth;
			Debug.Log ("Activate depth");
		}
	}

	// Use this for initialization
	void Start () 
	{
		if (SystemInfo.supportsImageEffects) 
		{
			//enabled = false;
			Debug.Log ("Image Effects not supported!");
			//return;
		}

		if (!pulseShader && !pulseShader.isSupported) 
		{
			Debug.Log ("Pulse shader not supported!");
			enabled = false;
		}

		if (!bloomShader && !bloomShader.isSupported) 
		{
			Debug.Log ("Bloom shader not supported!");
			enabled = false;
		}

		if (!maskShader && !maskShader.isSupported) 
		{
			Debug.Log ("Mask shader not supported!");
			enabled = false;
		}
	}

	void OnRenderImage(RenderTexture source, RenderTexture destination)
	{
		if (pulseShader == null || maskShader == null) 
		{
			Graphics.Blit (source, destination);
			return;
		}

		//Mask effect area (use destination renderTexture to maintain StencilBuffer!)
		Graphics.Blit (source, destination, mMaterial);

		RenderTexture rt = RenderTexture.GetTemporary (source.width, source.height, 0, RenderTextureFormat.RHalf);

		//copy effect to new render target
		Graphics.Blit (destination, rt);

		float widthMod = 0.5f;
		for(int i = 0; i < pulseBlurIterations; i++)
		{
			pMaterial.SetVector ("_Parameter", new Vector4 (pulseBlurSize * widthMod + (i*1.0f),0,0,0));

			// vertical blur
			RenderTexture rt2 = RenderTexture.GetTemporary (rt.width, rt.height, 0, rt.format);
			rt2.filterMode = FilterMode.Bilinear;
			Graphics.Blit (rt, rt2, pMaterial, 1);
			RenderTexture.ReleaseTemporary (rt);
			rt = rt2;

			// horizontal blur
			rt2 = RenderTexture.GetTemporary (rt.width, rt.height, 0, rt.format);
			rt2.filterMode = FilterMode.Bilinear;
			Graphics.Blit (rt, rt2, pMaterial, 2);
			RenderTexture.ReleaseTemporary (rt);
			rt = rt2;
		}

		switch (GlobalVariables.ammoType) 
		{
		case 1:
			pulseColor = color1;
			pattern = pattern1;
			patternSize = patternSize1;
			break;
		case 2:
			pulseColor = color2;
			pattern = pattern2;
			patternSize = patternSize2;
			break;
		case 3:
			pulseColor = color3;
			pattern = pattern3;
			patternSize = patternSize3;
			break;
		case 4:
			pulseColor = color4;
			pattern = pattern4;
			patternSize = patternSize4;
			break;
		}

		//blending
		if (pattern != null) {
			//int numberOfTiles = 15;
			//frame = (int)(Time.time * 25.0f) % numberOfTiles;
			//pMaterial.SetVector ("_PatternDim", new Vector4 ((float)numberOfTiles,1,0,0));
			//pMaterial.SetVector ("_PatternDim", new Vector4 (pattern.width / numberOfTiles,pattern.height,0,0));
			pMaterial.SetTexture ("_Pattern", pattern);
		}

		pMaterial.SetColor("_PulseColor", pulseColor);
		//pMaterial.SetVector ("_Parameter", new Vector4 (frame, pulseIntensity * 0.25f, uvShift / 50.0f, patternSize * 1000));
		pMaterial.SetVector ("_Parameter", new Vector4 (0, pulseIntensity * 0.25f, uvShift / 50.0f, patternSize * 1000));
		pMaterial.SetTexture ("_Mask", rt);

		if (useBloom) {

			RenderTexture temp = RenderTexture.GetTemporary (source.width, source.height, 8, source.format);

			Graphics.Blit (source, temp, pMaterial, 0);

			Bloom (temp, destination);

			RenderTexture.ReleaseTemporary (temp);
		} else {
			Graphics.Blit (source, destination, pMaterial, 0);
		}
		RenderTexture.ReleaseTemporary (rt);
	}

	void Bloom (RenderTexture source, RenderTexture destination)
	{
		if (bloomShader == null) 
		{
			Graphics.Blit (source, destination);
			return;
		}

		int divider = resolution == Resolution.Low ? 4 : 2;
		float widthMod = resolution == Resolution.Low ? 0.5f : 1.0f;

		bMaterial.SetVector ("_Parameter", new Vector4 (blurSize * widthMod, 0.0f, threshold, intensity));
		source.filterMode = FilterMode.Bilinear;

		var rtW= source.width/divider;
		var rtH= source.height/divider;

		// downsample
		RenderTexture rt = RenderTexture.GetTemporary (rtW, rtH, 0, source.format);
		rt.filterMode = FilterMode.Bilinear;
		Graphics.Blit (source, rt, bMaterial, 1);

		var passOffs= blurType == BlurType.Standard ? 0 : 2;

		for(int i = 0; i < blurIterations; i++)
		{
			bMaterial.SetVector ("_Parameter", new Vector4 (blurSize * widthMod + (i*1.0f), 0.0f, threshold, intensity));

			// vertical blur
			RenderTexture rt2 = RenderTexture.GetTemporary (rtW, rtH, 0, source.format);
			rt2.filterMode = FilterMode.Bilinear;
			Graphics.Blit (rt, rt2, bMaterial, 2 + passOffs);
			RenderTexture.ReleaseTemporary (rt);
			rt = rt2;

			// horizontal blur
			rt2 = RenderTexture.GetTemporary (rtW, rtH, 0, source.format);
			rt2.filterMode = FilterMode.Bilinear;
			Graphics.Blit (rt, rt2, bMaterial, 3 + passOffs);
			RenderTexture.ReleaseTemporary (rt);
			rt = rt2;
		}

		bMaterial.SetTexture ("_Bloom", rt);

		Graphics.Blit (source, destination, bMaterial, 0);

		RenderTexture.ReleaseTemporary (rt);
	}

	void OnDestroy ()
	{
		if (pulseMaterial) 
		{
			DestroyImmediate (pulseMaterial);
		}
	}
}
