﻿using UnityEngine;
using System.Collections;

public class Gate : MonoBehaviour {

	public int gateType;
	public GameObject particles;
	public bool isOpen=false;

	//sound related variables
	public AudioClip explosion;
	private AudioSource gateSource;



	// Use this for initialization
	void Start () {
		gateSource = GetComponent<AudioSource >();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OpenGate (){

		if (isOpen == false) {
			Debug.Log ("destroyed");

			foreach(Transform child in transform)
			{
				Destroy(child.gameObject);
			}
			Instantiate (particles, this.transform.position, this.transform.rotation);
			gateSource.PlayOneShot (explosion);
			//Destroy (this.gameObject);
			isOpen = true;
		}

	}

	public void getScore(){
		
		GlobalVariables.score += 50;
		Debug.Log ("gate passed");
		Destroy(this.gameObject);
	}
}
