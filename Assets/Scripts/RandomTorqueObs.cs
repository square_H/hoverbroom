﻿using UnityEngine;
using System.Collections;

public class RandomTorqueObs : MonoBehaviour {

	public int speed=10;
	private Rigidbody rb;
	// Use this for initialization
	void Start () {
	
		rb = GetComponent<Rigidbody>();
		float turn = Random.Range(-10.0f, 10.00f);
		rb.AddTorque(transform.up * turn * speed);
		 turn = Random.Range(-10.0f, 10.00f);
		rb.AddTorque(transform.right * turn  * speed);
		 turn = Random.Range(-10.0f, 10.00f);
		rb.AddTorque(transform.forward * turn  * speed);

		rb.AddForce(transform.forward * turn  * speed);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
