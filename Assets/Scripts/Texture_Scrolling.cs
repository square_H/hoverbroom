using UnityEngine;
using System.Collections;

public class Texture_Scrolling : MonoBehaviour {

 	public int materialIndex = 0;
    public Vector2 uvAnimationRate = new Vector2( 1f, 0.0f );
	private string textureName = "_MainTex";

    Vector2 uvOffset = Vector2.zero;

    void LateUpdate() 
    {
        uvOffset += ( uvAnimationRate * Time.deltaTime );
        if( GetComponent<Renderer>().enabled )
        {
            GetComponent<Renderer>().materials[ materialIndex ].SetTextureOffset( textureName, uvOffset );
        }
    }
}
