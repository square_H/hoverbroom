using UnityEngine;
using System.Collections;

public class Fly : MonoBehaviour {

	public float movementSpeed;
	public float turnSpeed;

    public AudioSource gatePassSound;

    public AudioSource soundGetAmmoTypeFire;
    public AudioSource soundGetAmmoTypeIce;
    public AudioSource soundGetAmmoTypeEarth;
    public AudioSource soundGetAmmoTypeElectric;

    public GameObject visor;

    void Start () {
        Debug.Log("Keyboard fly enabled.");
	}

	void Update () {
		
		//if(Input.GetKey("w")) {
			transform.position += transform.forward * Time.deltaTime * movementSpeed;
		//}
		//if(Input.GetKey("s")) {
		//	transform.position -= transform.forward * Time.deltaTime * movementSpeed;
		//}

		if(Input.GetKey("a")) {
			transform.Rotate(new Vector3(0,Time.deltaTime * -turnSpeed,0),Space.Self);
		}

		if(Input.GetKey("d")) {
			transform.Rotate(new Vector3(0,Time.deltaTime * turnSpeed,0),Space.Self);
		}

		if(Input.GetKey("w")) {
			transform.Rotate(new Vector3(Time.deltaTime * -turnSpeed,0 , 0),Space.Self);
		}

		if(Input.GetKey("s")) {
			transform.Rotate(new Vector3(Time.deltaTime * turnSpeed,0 , 0),Space.Self);
		}

        if (Input.GetKey("q"))
        {
            visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 0f, 0.05f));
        }


    }

	void OnTriggerEnter(Collider other) {

		if (other.tag == "Gate") {

			if (other.gameObject.GetComponent<Gate> ().isOpen == true) {
				//Destroy (other.gameObject);
				//Debug.Log ("Gate crossed");
				other.gameObject.SendMessage ("getScore");
				Debug.Log ("get passed");
                //PUT HERE THE SCORE CODE
                gatePassSound.Play();
            }   

		}

		if (other.tag == "Ammo") {

			Debug.Log ("enter Ammo");

			switch (other.GetComponent<Ammo> ().ammoType) {
			case 1:
                if (GlobalVariables.ammoType != 1)
                {
                    soundGetAmmoTypeFire.Play();
                    visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 0f, 0f, 0.05f));
                }
                GlobalVariables.ammoType = 1;
				break;
			case 2:
                if (GlobalVariables.ammoType != 2)
                {
                    soundGetAmmoTypeIce.Play();
                    visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(0f, 0.7f, 1f, 0.05f));
                }
				GlobalVariables.ammoType = 2;
				break;
			case 3:
                if (GlobalVariables.ammoType != 3)
                {
                    soundGetAmmoTypeEarth.Play();
                    visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(0f, 1f, 0f, 0.05f));
                }
				GlobalVariables.ammoType = 3;
				break;
			case 4:
                if (GlobalVariables.ammoType != 4)
                {
                    soundGetAmmoTypeElectric.Play();
                    visor.GetComponent<Renderer>().material.SetColor("_Color", new Color(1f, 1f, 0f, 0.03f));
                }
				GlobalVariables.ammoType = 4;
				break;
			}
		}

	}
}
