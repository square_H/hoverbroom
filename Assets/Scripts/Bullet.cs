﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	public float movementSpeed;
	public int bulletType;
	public GameObject particles;
	private GameObject aux;
	public GameObject effectVolume;


	// Use this for initialization
	void Start () {
		Destroy (this.gameObject, 3);
		aux = Instantiate (particles, this.transform.position, this.transform.rotation) as GameObject;
		aux.transform.parent = this.transform;


	}
	
	// Update is called once per frame
	void Update () {
	
		transform.position += transform.forward * Time.deltaTime * movementSpeed;
	}

	void OnTriggerEnter(Collider other) {

		if(other.tag != "Player") {
			Instantiate (effectVolume, this.transform.position, this.transform.rotation);
			Debug.Log ("Intstanciate Volume");
		}

		if (other.tag == "Gate") {

			if (other.GetComponent<Gate> ().gateType == 1 && bulletType == 1) {
				GlobalVariables.score += 10;
				other.gameObject.SendMessage ("OpenGate");

			}

			if (other.GetComponent<Gate> ().gateType == 2 && bulletType == 2) {
				GlobalVariables.score += 10;
				other.gameObject.SendMessage ("OpenGate");

			}

			if (other.GetComponent<Gate> ().gateType == 3 && bulletType == 3) {
				GlobalVariables.score += 10;
				other.gameObject.SendMessage ("OpenGate");

			}

			if (other.GetComponent<Gate> ().gateType == 4 && bulletType == 4) {
				GlobalVariables.score += 10;
				other.gameObject.SendMessage ("OpenGate");

			}

			Destroy (this.gameObject);

		}
	}

}
