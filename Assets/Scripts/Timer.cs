﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Timer : MonoBehaviour {

	public GameObject gameOverLabel;
	public int counter = 200;
	// Use this for initialization
	void Start () {
		
		StartCoroutine (Fade());
	
	}
	
	// Update is called once per frame
	void Update () {
		if (counter <= 0) {
			gameOverLabel.SetActive (true);
			UnityEngine.Time.timeScale = 0;

			this.gameObject.SetActive (false);
			//SceneManager.LoadScene (1);
		}
	}

	IEnumerator Fade(){
		while (counter > 0) {
			yield return new WaitForSeconds (1);
			counter--;
			GetComponent<TextMesh> ().text = "Time:"+ counter.ToString() ;

		}
	}
}
