﻿using UnityEngine;
using System.Collections;

public class SoundMixer : MonoBehaviour {

	public GameObject player;
	public AudioSource windSound;

	public float basePitch = 1f;
	public float pitchScale = 1f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		windSound.pitch = basePitch + pitchScale * Mathf.Sqrt(player.GetComponent<Rigidbody>().velocity.magnitude);
	}
}
