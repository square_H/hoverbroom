﻿Shader "Hoverbroom/StencilVolume" {
    SubShader {
        Tags {
        	"RenderType"="Opaque" 
        	"Queue"="Geometry+1"
        }
        ZWrite Off
 		ColorMask 0
 		Cull Off

 		Pass {
        	Stencil {
                Comp Always

                ZFailBack IncrWrap
                ZFailFront DecrWrap
            }
        }
    }
}