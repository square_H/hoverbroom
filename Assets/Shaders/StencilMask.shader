﻿Shader "Hoverbroom/StencilMask"
{
	CGINCLUDE
		#include "UnityCG.cginc"

		struct v2f_simple 
		{
			float4 pos : SV_POSITION;
		};	

		v2f_simple vert ( appdata_base v )
		{
			v2f_simple o;
			//v.vertex.xyz += v.normal * _SinTime;
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			return o; 
		}

		//set pixels to black
		fixed4 fragMaskBlack (v2f_simple i) : SV_Target
		{
			return 0;
		}
		//set pixels to white
		fixed4 fragMaskWhite (v2f_simple i) : SV_Target
		{
			return 1;
		}
	ENDCG

	SubShader
	{
		// No culling or depth
		Cull Off 
		ZWrite Off 
		ZTest Always
		Fog { Mode Off }

		Pass
		{
			Stencil {
				Ref 0
	            Comp Equal
	        }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragMaskBlack

			ENDCG
		}
		Pass
		{
			Stencil {
				Ref 0
	            Comp NotEqual
	        }

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment fragMaskWhite

			ENDCG
		}
	}
}
