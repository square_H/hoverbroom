﻿Shader "Hoverbroom/Pulse"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Mask ("Pulse (RGB)", 2D) = "black" {}
		_Pattern ("Pattern (RGB)", 2D) = "black" {}

		_PulseColor ("Pulse Color", Color) = (1,1,1,1)
	}

	CGINCLUDE
		#include "UnityCG.cginc"

		uniform sampler2D_float _CameraDepthTexture;

		sampler2D _MainTex;
		sampler2D _Mask;
		sampler2D _Pattern;

		half4 _PulseColor;

		uniform half4 _MainTex_TexelSize;
		half4 _MainTex_ST;
		
		uniform half4 _Parameter;
		//uniform half4 _PatternDim;

		struct v2f_simple 
		{
			float4 pos : SV_POSITION; 
			half2 uv : TEXCOORD0;
			half2 uvPattern : TEXCOORD1;

//			#if UNITY_UV_STARTS_AT_TOP
//				half2 uv2 : TEXCOORD1;
//			#endif
		};

		v2f_simple vertBlend ( appdata_base v )
		{
			v2f_simple o;

			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
	    	o.uv = UnityStereoScreenSpaceUVAdjust(v.texcoord, _MainTex_ST);
	    	
//	   		#if UNITY_UV_STARTS_AT_TOP
//
//	    	o.uv2 = o.uv;
//	    	if (_MainTex_TexelSize.y < 0.0)
//	    		o.uv.y = 1.0 - o.uv.y;
//
//	    	#endif

			o.uvPattern = o.uv;

//			float patternSize = _Parameter.w;
//			float frame = _Parameter.x;
//
//    		o.uvPattern.x = (v.texcoord.x / _PatternDim.x + (1.0f / _PatternDim.x * frame));// * patternSize;
//    		o.uvPattern.y = (v.texcoord.y / _PatternDim.y + (1.0f / _PatternDim.y));// * patternSize;
	    	        	
			return o; 
		}

		fixed4 fragBlend ( v2f_simple i ) : SV_Target
		{
			//UnityStereoTransformScreenSpaceTex(i.ui)

			//return tex2D(_Pattern, i.uvPattern);

			float intensity = _Parameter.y;
			float shift = _Parameter.z;
			float patternSize = _Parameter.w;

			half mask = tex2D(_Mask, i.uv).r;
			fixed4 color = tex2D(_MainTex, i.uv+ half2(-shift, -shift) * 2.0f * mask);
			float rawDepth = SAMPLE_DEPTH_TEXTURE(_CameraDepthTexture, i.uv);
			float dpth = Linear01Depth(rawDepth);

			half2 uvPattern = i.uvPattern * dpth * patternSize + half2(0.2, 0.2) * _Time;
			//uvPattern.x = fmod(uvPattern.x * 100.0f, _Parameter.x) / 100.0f;// + 32 * fmod(_Time, );
			//uvPattern.x = uvPattern.x / tiles.x + (c_one / tiles.x * tile.x);

			fixed4 patternTex = tex2D(_Pattern, uvPattern);

			//Edge detection
			half delta;
			delta = abs(mask - tex2D(_Mask, i.uv + half2(-1.0f/_ScreenParams.x, 0)).r);
			delta += abs(mask - tex2D(_Mask, i.uv + half2(0, -1.0f/_ScreenParams.y)).r);

			//return delta;

			color *= (1-delta) * (1-patternTex * mask);
			fixed4 border = _PulseColor * delta;
			fixed4 pattern = patternTex * _PulseColor * mask * (1-delta);
			fixed4 filling = _PulseColor * intensity * mask;

			return color + border + pattern + filling;
		}

		// ------------------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------------------
		// ------------------------------------from MobileBloom.shader-------------------------------------------------
		// ------------------------------------------------------------------------------------------------------------
		// ------------------------------------------------------------------------------------------------------------
	
		// weight curves
		static const half4 curve4[7] = { half4(0.0205,0.0205,0.0205,0), half4(0.0855,0.0855,0.0855,0), half4(0.232,0.232,0.232,0),
			half4(0.324,0.324,0.324,1), half4(0.232,0.232,0.232,0), half4(0.0855,0.0855,0.0855,0), half4(0.0205,0.0205,0.0205,0) };

		struct v2f_withBlurCoords8 
		{
			float4 pos : SV_POSITION;
			half4 uv : TEXCOORD0;
			half2 offs : TEXCOORD1;
		};

		v2f_withBlurCoords8 vertBlurHorizontal (appdata_img v)
		{
			v2f_withBlurCoords8 o;
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			
			o.uv = half4(v.texcoord.xy,1,1);
			o.offs = _MainTex_TexelSize.xy * half2(1.0, 0.0) * _Parameter.x;

			return o; 
		}
		
		v2f_withBlurCoords8 vertBlurVertical (appdata_img v)
		{
			v2f_withBlurCoords8 o;
			o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
			
			o.uv = half4(v.texcoord.xy,1,1);
			o.offs = _MainTex_TexelSize.xy * half2(0.0, 1.0) * _Parameter.x;
			 
			return o; 
		}	

		half4 fragBlur8 ( v2f_withBlurCoords8 i ) : SV_Target
		{
			half2 uv = i.uv.xy; 
			half2 netFilterWidth = i.offs;  
			half2 coords = uv - netFilterWidth * 3.0;  
			
			half4 color = 0;
  			for( int l = 0; l < 7; l++ )  
  			{   
				half4 tap = tex2D(_MainTex, UnityStereoScreenSpaceUVAdjust(coords, _MainTex_ST));
				color += tap * curve4[l];
				coords += netFilterWidth;
  			}
			return color;
		}
	ENDCG

	SubShader
	{
		// No culling or depth
		Cull Off 
		ZWrite Off 
		ZTest Always
		Fog { Mode Off }

		//0 Blend effect
		Pass {
			CGPROGRAM
			#pragma vertex vertBlend
			#pragma fragment fragBlend
			
			ENDCG
			}
		//1 vertical blur from MobileBloom.shader
		Pass {
			CGPROGRAM 
			
			#pragma vertex vertBlurVertical
			#pragma fragment fragBlur8
			
			ENDCG 
			}
		//2 horizontal blur from MobileBloom.shader
		Pass {	
			CGPROGRAM
			
			#pragma vertex vertBlurHorizontal
			#pragma fragment fragBlur8
			
			ENDCG
			}
		}
}
